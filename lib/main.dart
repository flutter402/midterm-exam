import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'NavBar.dart';

void main() {
  runApp(const ColumnExample());
}

class ColumnExample extends StatelessWidget {
  const ColumnExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Register Burapha University',
        theme: ThemeData(
          // primarySwatch: Colors.grey,

        ),
        home: Scaffold(
          resizeToAvoidBottomInset: false,
          drawer: NavBar(),
          backgroundColor: Colors.greenAccent[100],
          appBar: AppBar(
            title: const Text('Main page'),
            backgroundColor: Colors.black,
          ),
          body:SafeArea(
            //Maybe use SingleChildScrollView, ListView , CustomScrollView
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Hi, Suraida!',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text('1 Feb, 2023',
                              style: TextStyle(color: Colors.green))
                        ],
                      ),

                      Container(
                        decoration: BoxDecoration(
                          color: Colors.greenAccent,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        padding: EdgeInsets.all(12),
                        child: Icon(
                          Icons.notifications,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),

                        Container(
                          decoration: BoxDecoration(
                            color: Colors.greenAccent,
                            borderRadius: BorderRadius.circular(12)
                          ),
                          padding: EdgeInsets.all(12),
                          child: Row(
                            children: [
                              Icon(
                                Icons.search,
                                color: Colors.white,
                              ),
                              Text(
                                'Search',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              )
                            ],
                          ),
                        ),

                  SizedBox(
                    height: 30,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'วันนี้เป็นยังไงบ้าง?',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 25,
                        ),
                      ),
                      Icon(
                        Icons.more_horiz,
                        color: Colors.black,
                      )
                    ],
                  ),

                  SizedBox(
                    height: 30,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.greenAccent,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        padding: EdgeInsets.all(20),
                        child: Icon(
                          Icons.emoji_emotions,
                          color: Colors.white,
                        ),

                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.greenAccent,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        padding: EdgeInsets.all(20),
                        child: Icon(
                          Icons.emoji_emotions,
                          color: Colors.white,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.greenAccent,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        padding: EdgeInsets.all(20),
                        child: Icon(
                          Icons.emoji_emotions,
                          color: Colors.white,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.greenAccent,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        padding: EdgeInsets.all(20),
                        child: Icon(
                          Icons.emoji_emotions,
                          color: Colors.white,
                        ),

                      ),

                    ],
                  ),

                  SizedBox(
                    height: 30,
                  ),

                  Expanded(
                      child: Container(
                        padding: EdgeInsets.all(25),
                        color: Colors.grey[300],
                        child: Center(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'ประจำวันนี้',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 25,
                                    ),
                                  ),
                                  Icon(Icons.more_horiz),
                                ],
                              ),

                              SizedBox(
                                height: 20,
                              ),

                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                child: ListTile(
                                  leading: Icon(Icons.backup_table),
                                  title: Text('ตารางเรียน'),
                                  subtitle: Text('10:00 น. วิชา: Mobile Application'),
                                ),
                              ),

                              SizedBox(
                                height: 20,
                              ),

                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                child: ListTile(
                                  leading: Icon(Icons.event_note_rounded),
                                  title: Text('กิจกรรมเร็วๆนี้'),
                                  subtitle: Text('พรุ่งนี้ 13:00 น. อบรม'),
                                ),
                              ),

                              SizedBox(
                                height: 20,
                              ),

                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                child: ListTile(
                                  leading: Icon(Icons.schedule),
                                  title: Text('รายการแนะนำ'),
                                  subtitle: Text('กีฬาบูรพาเริ่มต้นขึ้นแล้ว'),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ))
                ],
              )
            )
          ),
        ),
      ),
    );
  }
}