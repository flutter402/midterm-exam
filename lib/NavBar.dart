import 'package:flutter/material.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Remove padding
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('ซูไรดา บินเยาะ'),
            accountEmail: Text('คณะวิทยาการสารสนเทศ'),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network(
                  "https://cdn.donmai.us/original/6d/be/6dbe40c98d597939341331036a3444e0.jpg",
                  fit: BoxFit.cover,
                  width: 100,
                  height: 100,
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.grey.shade800,
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                "https://cdn.pixabay.com/photo/2016/12/16/06/18/green-smoke-1910658_960_720.jpg",
                      )),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home,color: Colors.green),
            title: Text('MAINPAGE'),
          ),
          ListTile(
            leading: Icon(Icons.table_view,color: Colors.green),
            title: Text('ENROLL IN'),
          ),
          ListTile(
            leading: Icon(Icons.backup_table,color: Colors.green),
            title: Text('CLASS SCHEDULE'),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.badge,color: Colors.green),
            title: Text('PERSONAL'),
          ),
          ListTile(
            leading: Icon(Icons.person_pin_rounded,color: Colors.green),
            title: Text('STUDIED RESULT'),
          ),
          ListTile(
            leading: Icon(Icons.attach_money,color: Colors.green),
            title: Text('COST BURDEN'),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.file_open,color: Colors.green),
            title: Text('PETITION'),
          ),
          ListTile(
            leading: Icon(Icons.settings,color: Colors.green),
            title: Text('SETTING'),
          ),
          ListTile(
            leading: Icon(Icons.logout,color: Colors.green),
            title: Text('LOG OUT'),
          ),
        ],
      ),
    );
  }
}
